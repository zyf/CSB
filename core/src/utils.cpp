#include "utils.h"
#include "coord.h"
#include "csbmath.h"
#include "csnode.h"

#ifdef __cplusplus
extern "C" {
#endif
/*********************| extern data |*********************/
CSB_DLL float _vworld_w = 480.0f;
CSB_DLL float _vworld_h = 800.0f;
CSB_DLL area_t _csb_screen = {0, 0, _vworld_w, _vworld_h};

/*********************| extern functions |*********************/
CSB_DLL bool csb_common_init(float w, float h)
{
	COORD_CONFIG_VIEW_WORLD(w, h);
	CSB_CONFIG_SCREEN(0, 0, w, h);
	CSB_math_init();
	list_buf_init();
	csnode_buf_init();
	CSB_NO_RET(CSBERR_NOERR == csberrno, false);
	csobj_buf_init();
	CSB_NO_RET(CSBERR_NOERR == csberrno, false);
	return true;
}

CSB_DLL void csb_common_end(bool force)
{
	csnode_buf_end(force);
	csobj_buf_end(force);
}

#ifdef __cplusplus
} /* end extern C */
#endif
