#include "csobj.h"
#include "utils.h"
#include "emitter.h"
#include "laser.h"

#ifdef __cplusplus
extern "C" {
#endif
/*********************| static data |*********************/
static bool		_obj_buf_inited = false;

/*********************| extern functions |*********************/

CSB_DLL csobjdata_t* csobjdata_read(const int type, unsigned char* data, const uint32_t totalframe)
{
	csberrno = CSBERR_NOERR;
	CSB_YES_ERR_RET(!data, CSBERR_PARAM, NULL);
	switch (type) {
	case EMITTER:
		return emitterdata_read(data, totalframe);
	case LASER:
		return laserdata_read(data, totalframe);
	default:
		csberrno = CSBERR_PARAM;
		return NULL;
	}
}


CSB_DLL void csobjdata_free(csobjdata_t* pobj)
{
	csberrno = CSBERR_NOERR;
	CSB_YES_ERR_VRET(!pobj, CSBERR_PARAM);
	switch (pobj->type) {
	case EMITTER:
		emitterdata_free(pobj);
		return;
	case LASER:
		laserdata_free(pobj);
		return;
	default:
		csberrno = CSBERR_PARAM;
		return;
	}
}

CSB_DLL csobj_t* csobjdata_make_obj(const csobjdata_t* src)
{
	csberrno = CSBERR_NOERR;
	CSB_YES_ERR_RET(!src, CSBERR_PARAM, NULL);
	switch (src->type) {
	case EMITTER:
		return (csobj_t*)emitterdata_make_obj(src);
	case LASER:
		return (csobj_t*)laserdata_make_obj(src);
	default:
		csberrno = CSBERR_PARAM;
		return NULL;
	}
}

CSB_DLL void csobj_buf_init()
{
	CSB_YES_VRET(_obj_buf_inited);
	action_eff_buf_init();
	action_buf_init();
	emitter_buf_init();
	laser_buf_init();
	_obj_buf_inited = true;
}


CSB_DLL void csobj_buf_end(bool force)
{
	csberrno = CSBERR_NOERR;
	CSB_NO_ERR_VRET(_obj_buf_inited, CSBERR_SYSNOINITED);
	emitter_buf_end(force);
	laser_buf_end(force);
	action_buf_end(force);
	_obj_buf_inited = false;
}


CSB_DLL csobj_t* csobj_buf_rent(const int type)
{
	csberrno = CSBERR_NOERR;
	CSB_NO_ERR_RET(_obj_buf_inited, CSBERR_SYSNOINITED, NULL);
	switch (type) {
	case EMITTER:
		return (csobj_t*)emitter_buf_rent();
	case LASER:
		return (csobj_t*)laser_buf_rent();
	default:
		csberrno = CSBERR_PARAM;
		return NULL;
	}
}

CSB_DLL void csobj_buf_return(csobj_t* pobj, const int type)
{
	csberrno = CSBERR_NOERR;
	CSB_NO_ERR_VRET(_obj_buf_inited, CSBERR_SYSNOINITED);
	CSB_NO_ERR_VRET(pobj, CSBERR_PARAM);
	switch (type) {
	case EMITTER:
		emitter_buf_return((emitter_t*)pobj);
		return;
	case LASER:
		laser_buf_return((laser_t*)pobj);
		return;
	default:
		csberrno = CSBERR_PARAM;
		return;
	}
}


CSB_DLL void csobj_set_rotate(csobj_t* pobj, const int16_t d, const int type)
{
	csberrno = CSBERR_NOERR;
	CSB_NO_ERR_VRET(pobj, CSBERR_PARAM);
	switch(type) {
	case EMITTER:
		emitter_set_rotate((emitter_t*)pobj, d);
		return;
	case LASER:
		// TODO: 激光旋转的设定
		return;
	default:
		csberrno = CSBERR_PARAM;
		return;
	}
}

#ifdef __cplusplus
} // end of extern C
#endif
