#include <errno.h>
#include <string.h>
#include "err.h"

#ifdef __cplusplus
extern "C" {
#endif
/*********************| extern data |*********************/
CSB_DLL int csberrno = CSBERR_NOERR;

/*********************| extern functions |*********************/

CSB_DLL const char*csb_errmsg(const int err)
{
	if ((const uint32_t)err == CSBERR_FINIAL)
		return "达到数值边界，也许超过了某些有效范围的定义了";

	switch (err) {
	case CSBERR_SYSERR:
#ifdef _MSC_VER
		return strerror(errno);
#else
		return "操作系统内部错误";
#endif
	case CSBERR_NOERR:
		return "没有发生错误";
	case CSBERR_OPENFILE:
		return "打开文件失败";
	case CSBERR_FILETOOLARGE:
		return "文件体积过大";
	case CSBERR_FILETYPE:
		return "文件类型错误, 或者版本号不符";
	case CSBERR_PARAM:
		return "参数错误";
	case CSBERR_SYSNOINITED:
		return "系统未被初始化";
	case CSBERR_BUF_NOT_ENOUGH:
		return "缓冲不足";
	default:
		return "未知错误";
	}
}

CSB_DLL const char*csb_lasterr()
{
	return csb_errmsg(csberrno);
}

#ifdef __cplusplus
} // end of extern C
#endif
