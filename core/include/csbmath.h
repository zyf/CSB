#ifndef __CSBMATH_H_
#define __CSBMATH_H_

/**
 * Desc: 一些用于计算的数学函数(摘自doom3.gpl引擎。感谢开源世界，感谢idsoft)
 * Auth: 张宇飞
 * Date: 2014-10-21    
 */
#include "config.h"
#include <math.h>
#include "csbtypes.h"

#ifdef __cplusplus
extern "C" {
#endif

#define	FLOAT_IS_NAN(x)			(((*(const unsigned long *)&x) & 0x7f800000) == 0x7f800000)
#define FLOAT_IS_INF(x)			(((*(const unsigned long *)&x) & 0x7fffffff) == 0x7f800000)
#define FLOAT_IS_IND(x)			((*(const unsigned long *)&x) == 0xffc00000)
#define	FLOAT_IS_DENORMAL(x)		(((*(const unsigned long *)&x) & 0x7f800000) == 0x00000000 && \
		((*(const unsigned long *)&x) & 0x007fffff) != 0x00000000 )

#define CSB_DEG2RAD(_an_)		((_an_) * 0.01745329252f)//角度到弧度
#define CSB_RAD2DEG(_ra_)		((_ra_) * 57.29577951f)//弧度到角度

extern CSB_DLL void			CSB_math_init();
extern CSB_DLL float			CSB_sqrt(float x);
extern CSB_DLL int			CSB_abs(int x);
extern CSB_DLL float			CSB_fabs(float x);
extern CSB_DLL float			CSB_angle_normalize_360(float x);
extern CSB_DLL float			CSB_angle_normalize_180(float x);
extern CSB_DLL float			CSB_angle_delta(float a, float b);
#define CSB_pow				powf
#define CSB_cos				cosf
#define CSB_sin				sinf
#define CSB_tan				tanf
#define CSB_atan			atanf
#define CSB_acos			acosf
#define CSB_asin			asinf
#define CSB_fmod			fmodf
extern CSB_DLL void			CSB_rotate(float* x, float* y, float degree);
extern CSB_DLL float			CSB_rotate_p2p(const pos_t* ori, const pos_t* target);
#define CSB_distance(x1, y1, x2, y2)	CSB_sqrt(CSB_pow(y2-y1, 2) + CSB_pow(x2-x1, 2))

#ifdef __cplusplus
} /* end extern C */
#endif

#endif /* end of include guard: __CSBMATH_H_ */
