#ifndef COORD_H
#define COORD_H
/**
 * Desc: 处理坐标系
 * Auth: 张宇飞
 * Date: 2014-08-21
 */
#include "config.h"

#ifdef __cplusplus
extern "C" {
#endif

#define COORD_CONFIG_VIEW_WORLD(w, h)	do{_vworld_w = (w);_vworld_h = (h);}while(0)
#define IN_COORD_WORLD(_pos_) \
	(_pos_.x >=0 && _pos_.x <= _vworld_w && _pos_.y >= 0 && _pos_.y <= _vworld_h)

/* 屏幕坐标转换 */
#if (COORD_SYS_ORIGN == COORD_LD)
#define COORD_X_CONVERT(_x_)	 _x_
#define COORD_Y_CONVERT(_y_)	 _vworld_h - _y_
#define COORD_VAX_CONVERT(_vx_) (_vx_)
#define COORD_VAY_CONVERT(_vy_) -(_vy_)
#else
#define COORD_X_CONVERT(_x_)	_x_
#define COORD_Y_CONVERT(_y_)	_y_
#define COORD_VAX_CONVERT(_vx_) (_vx_)
#define COORD_VAY_CONVERT(_vy_) (_vy_)
#endif

#define COORD_POS_CONVERT(_pos_,_res_)	\
	_res_.x = COORD_X_CONVERT(_pos_.x);\
	_res_.y = COORD_Y_CONVERT(_pos_.y)

#ifdef __cplusplus
} /* end extern C */
#endif

#endif // COORD_H
