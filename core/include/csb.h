#ifndef __CSB_H_
#define __CSB_H_

/**
 * Desc: csb文件的数据格式描述
 * Auth: 张宇飞
 * Date: 2014-07-29    
 */

#include "config.h"
#include "csobj.h"
#include "err.h"

#ifdef __cplusplus
extern "C" {
#endif

/* mbg文件大小不能超过这个值 */
#define FILE_BUF_SIZE	1024*1024

/**
 * @brief load_csbdata 从数据直接加载
 */
extern CSB_DLL list_t*	load_csbdata(unsigned char* pdata);
/**
 * @brief 读取csbfpath指定目录的csb文件，返回其中包含的对象数量和对象数组指针
 */
extern CSB_DLL list_t* 	load_csb(const char* csbfpath);

#ifdef __cplusplus
} /* end extern C */
#endif

#endif /* end of include guard: __CSB_H_ */
