#ifndef CSBCONFIG_H
#define CSBCONFIG_H

/**
 * Desc: 配置软件的属性
 * Auth: 张宇飞
 * Date: 2014-09-25    
 */
#ifdef __cplusplus
extern "C" {
#endif

/*********************| 配置宏 |*********************/

#define CSB_USE_DLL	1	// 是否使用动态库

#ifdef _MSC_VER
	#ifdef CSB_USE_DLL
		#define CSB_DLL		__declspec(dllexport)
	#else
		#define CSB_DLL		__declspec(dllimport)
	#endif // CSB_USE_DLL
#else
	#define CSB_DLL			__attribute__((visibility("default")))
#endif // _MSC_VER

//#define CSB_DEBUG	1	// 调试(日志)开关

/* 从csb读出来的数据是屏幕左上角是原点的 */
#define COORD_LT	0	// 屏幕左上角
#define COORD_LD	1	// 屏幕左下角
#define COORD_SYS_ORIGN	COORD_LD// 坐标原点的配置

#define ACTION_BUF_SIZE		4096 // 事件结构缓冲
#define ACTION_EFF_BUF_SIZE	8192 // 事件结果缓冲
#define EMITTER_BUF_SIZE	127 // 发射器数量缓冲
#define EMITTER_BULLET_BUF_SIZE	2047 // 发射器子弹数量缓冲
#define LASER_BUF_SIZE		1 // 激光数量缓冲
#define LASER_BULLET_BUF_SIZE	1 // 激光子弹的缓冲
#define CSNODE_BUF_SIZE		(LASER_BUF_SIZE + EMITTER_BUF_SIZE) // 运算节点缓冲
#define LIST_NODE_BUF_SIZE	(ACTION_BUF_SIZE + ACTION_EFF_BUF_SIZE + EMITTER_BUF_SIZE + EMITTER_BULLET_BUF_SIZE + LASER_BUF_SIZE + LASER_BULLET_BUF_SIZE + CSNODE_BUF_SIZE) * 2

//#define CSNODE_ENALBE_ONFRAME_CALL	1 // 是否开启帧回调(每帧都会执行一次的接口

#ifdef __cplusplus
} /* end extern C */
#endif

#endif // CSBCONFIG_H
