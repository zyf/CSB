#ifndef __CSOBJ_H_
#define __CSOBJ_H_

/**
 * Desc: 用来定义元素基础属性
 * Auth: 张宇飞	
 * Date: 2014-08-29
 */
#include "config.h"
#include "csbtypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/* 允许使用的元素类型 */
#define EMITTER		0
#define LASER		1
/* 特殊标签 */
#define BODY_SELF	-99998	//自身
#define AUTO_SELF	-99999	//自机
#define ASSIGN_POS	-100000	//指定坐标

#define NO_BODY_SELF(_pos_)	(BODY_SELF != _pos_.x && BODY_SELF != _pos_.y)
/* 自机的控制标志位 */
#define R_AM	0x1
#define X_AM	0x2
#define Y_AM	0x4
#define RB_AM	0x8
#define RBRT_AM	0x10

/* 子弹特效的比特位 */
#define BU_EFF_Atom	0x1	//雾化效果
#define BU_EFF_Fade	0x2	//消除效果
#define BU_EFF_Light	0x4	//高光效果
#define BU_EFF_Smear	0x8	//拖影效果
#define BU_EFF_OutSc	0x10	//出屏即消
#define BU_EFF_Invin	0x20	//无敌状态
#define BU_EFF_Face2V	0x30	//朝向总是与速度方向相同

/* CrazyStorm 内置元素基本属性数据 */
typedef struct {
	uint32_t	totalframe;	//后来补充的(读取数据的时候特殊处理)
	int32_t		type;		//元素类型
	char		name[16];	//元素的名字(不需要自己补0)
	uint32_t	fs;		//起始播放帧
	uint32_t	fe;		//结束播放帧
	emitEdge_t	emitedge;	//发射器半径和旋转角度
	emitEdge_t	emitedgeVar;	//发射器半径和旋转角度随机量
	uint32_t	freq;		//发射周期
	uint32_t	freqVar;	//发射周期随机量
	v_t		v;		//速度
	v_t		vVar;		//速度随机量
	v_t		va;		//加速度
	v_t		vaVar;		//加速度随机量
	float		angle;		//发射起始方向旋转角度
	float		angleVar;	//发射起始方向旋转角度随机量
	int16_t		emitrange;	//射击范围
	int16_t		emitrangeVar;	//射击范围随机量
	uint16_t	nmuzzle;	//炮口数量
	uint16_t	nmuzzleVar;	//炮口数量随机量
} csobjdata_t;

/* 子弹通用信息结构数据 */
typedef struct {
	int32_t		ID;	//类型
	uint32_t	life;	//生命
	float		v;	//速度
	float		vVar;	//速度随机量
	v_t		va;	//加速度
	v_t		vaVar;	//加速度随机量
	scale_t		scale;
} bulletdata_t;

typedef struct {
	int32_t			ID;
	uint32_t		life;
	uint32_t		lifetime;
	v_t			v;
	v_t			va;
	scale_t			vRate;
} bullet_attr_t;

typedef struct {
	elebase_t		ele;
	size_t			life;
	float			rotateOffset; //角度修正
	uint32_t		freq;
	v_t			v;
	v_t			va;
	emitEdge_t		emitedge;
	int16_t			emitRange;
	uint16_t		nmuzzle;
	list_t			ac_eff_list;
} csobj_t;

typedef struct {
	elebase_t		ele;
	bullet_attr_t		attr;
	list_t			ac_eff_list;
	void*			udata;// 用户自定义数据。
} bullet_t;

typedef struct node_cb_t	node_cb_t;
/*********************| 外部接口 |*********************/

extern CSB_DLL csobjdata_t*	csobjdata_read(const int type, unsigned char* data, const uint32_t totalframe);
extern CSB_DLL void		csobjdata_free(csobjdata_t* pobj);
extern CSB_DLL csobj_t*		csobjdata_make_obj(const csobjdata_t* src);

extern CSB_DLL void		csobj_buf_init();
extern CSB_DLL void		csobj_buf_end(bool force);
extern CSB_DLL csobj_t*		csobj_buf_rent(const int type);
extern CSB_DLL void		csobj_buf_return(csobj_t* pobj, const int type);

extern CSB_DLL void		csobj_set_rotate(csobj_t* pobj, const int16_t d, const int type);


#ifdef __cplusplus
} /* end extern C */
#endif

#endif /* end of include guard: __CSOBJ_H_ */
