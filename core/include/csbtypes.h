#ifndef __CSBTYPES_H_
#define __CSBTYPES_H_

/**
 * Desc: 公共数据类型定义
 * Auth: 张宇飞
 * Date: 2014-08-01    
 */

#include <stdint.h>
#include <stdlib.h>
#include "list.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _MSC_VER
typedef long int ssize_t;
#endif
/* 坐标点 */
typedef struct {
	float	x;
	float	y;
}pos_t;

/* 区域 */
typedef struct {
	float	lx;
	float	ly;
	float	rx;
	float	ry;
}area_t;

/* 发射半径和旋转角度 */
typedef struct {
	float	r;
	float	angle;
} emitEdge_t;

/* 缩放 */
typedef struct {
	float		xs;
	float		ys;
} scale_t;

/* 速度和速度方向 */
typedef struct {
	float	v;
	float	dir;
} v_t;

/* 颜色定义 */
typedef struct {
	uint8_t	R;
	uint8_t	G;
	uint8_t	B;
	uint8_t	A;
} color_t;

/* 元素是传递给外部plugin信息的基础，也是计算结果的存放处 */
typedef struct {
	pos_t	pos;
	scale_t scale;
	float	rotate;
	int	flag;	// 自定义数据
} elebase_t;
#ifdef __cplusplus
} /* end extern C */
#endif
#endif /* end of include guard: __CSBTYPES_H_ */
