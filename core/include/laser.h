#ifndef __LASER_H_
#define __LASER_H_

/**
 * Desc: 激光的解析
 * Auth: 张宇飞
 * Date: 2014-07-29    
 */

#include "config.h"
#include "csobj.h"
#include "action.h"
#include "csnode.h"

#ifdef __cplusplus
extern "C" {
#endif

/*********************| 数据声明 |*********************/
typedef struct {
	bullet_t	b;
	int		effect;
	ac_t		aclist;
} laserbu_t;

/* 激光子弹元素 */
typedef struct {
	csobj_t		obj;
	bullet_attr_t	buattr;
	int		bueffect;
	scale_t		buScale;
	ac_t		aclist;
} laser_t;


/*********************| static data |*********************/
typedef struct {
	bulletdata_t	b;
	bool		   startX;	//使用射线激光吗
	color_t		color;		//颜色和alpha
	scale_t		vRate;		//速度缩放倍率
	int32_t		effect;		//特效
	ac_t		aclist;		//子弹事件
} laserbudata_t;

typedef struct {
	csobjdata_t	objtag;		//元素标签
	pos_t		emitpos;	//发射坐标
	pos_t		emitPosVar; 	//坐标随机量
	ac_t		aclist;		//事件
	laserbudata_t	bullet;	// 子弹
}laserdata_t;


/*********************| 外部接口 |*********************/
extern CSB_DLL csobjdata_t*	laserdata_read(unsigned char* data, const uint32_t totalframe);
extern CSB_DLL laser_t* 	laserdata_make_obj(const csobjdata_t* pdata);
extern CSB_DLL void		laserdata_free(csobjdata_t* plaser);

extern CSB_DLL void		laser_buf_init();
extern CSB_DLL void		laser_buf_end(bool force);
extern CSB_DLL laser_t*		laser_buf_rent();
extern CSB_DLL void		laser_buf_return(laser_t* pla);
extern CSB_DLL void		laser_emit(csnode_t* pcsn);
extern CSB_DLL void		laserbu_buf_return(laserbu_t* pbu);
extern CSB_DLL void 	laser_update(csnode_t* pcsn, bool updateObj, bool updateBullet);

#ifdef __cplusplus
} /* end extern C */
#endif

#endif /* end of include guard: __LASER_H_ */
