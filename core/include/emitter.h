#ifndef __EMITTER_H_
#define __EMITTER_H_

/**
 * Desc: 发射器的解析
 * Auth: 张宇飞
 * Date: 2014-07-29    
 */

#include "config.h"
#include "csobj.h"
#include "action.h"
#include "csnode.h"

#ifdef __cplusplus
extern "C" {
#endif

/*********************| data |*********************/

typedef struct {
	bullet_t	b;
	color_t		color;
	int		effect;
	ac_t		aclist;
}emitterbu_t;

/* 发射器子弹元素 */
typedef struct {
	csobj_t		obj;
	bullet_attr_t	buattr;
	color_t		bucolor;
	int		bueffect;
	scale_t		buScale;
	ac_t		aclist;
} emitter_t;
/*********************| 外部接口 |*********************/

extern CSB_DLL csobjdata_t*	emitterdata_read(unsigned char* data, const uint32_t totalframe);
extern CSB_DLL emitter_t* 	emitterdata_make_obj(const csobjdata_t* pdata);
extern CSB_DLL void		emitterdata_free(csobjdata_t* pemitter);

extern CSB_DLL void		emitter_buf_init();
extern CSB_DLL void		emitter_buf_end(bool force);
extern CSB_DLL emitter_t*	emitter_buf_rent();
extern CSB_DLL void		emitter_buf_return(emitter_t* pem);

extern CSB_DLL void		emitter_emit(csnode_t* pcsn);
extern CSB_DLL void		emitterbu_buf_return(emitterbu_t* pbu);
extern CSB_DLL void		emitter_update(csnode_t* pcsn, bool updateObj, bool updateBullet);

extern CSB_DLL void		emitter_set_rotate(emitter_t* pem, const float d);

#ifdef __cplusplus
} /* end extern C */
#endif

#endif /* end of include guard: __EMITTER_H_ */
