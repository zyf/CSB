#ifndef __ERR_H_
#define __ERR_H_

/**
 * Desc: 描述错误码的
 * Auth: 张宇飞
 * Date: 2014-08-04    
 */
 
#include <stdint.h>
#include "config.h"
#ifdef __cplusplus
extern "C" {
#endif

/*********************| 外部变量和宏 |*********************/
/* 错误码 */
extern CSB_DLL int 		csberrno;
/* TODO:错误码列表 */
#define CSBERR_NOERR		0
#define CSBERR_SYSERR		1
#define CSBERR_OPENFILE		2
#define CSBERR_FILETOOLARGE	3
#define CSBERR_FILETYPE		4
#define CSBERR_PARAM		5
#define CSBERR_SYSNOINITED	6
#define CSBERR_BUF_NOT_ENOUGH	7
#define CSBERR_FINIAL		((uint32_t)(-1))
#define TEST_ERR_GOTO(_p_, _e_, _h_)	if ((_p_)) {csberrno = _e_; goto _h_;}

/*********************| 外部接口 |*********************/
extern CSB_DLL const char*	csb_errmsg(const int err);
extern CSB_DLL const char*	csb_lasterr();

#ifdef __cplusplus
} /* end extern C */
#endif
#endif /* end of include guard: __ERR_H_ */
