第一行是CS版本

第二行描述中心的运动:
	关键字:Center
	参数意义:中心x坐标，中心y坐标，起始速度，起始速度方向，起始加速度，起始加速度方向
	(其中速度方向表示的是顺时针方向为+，逆时针方向为-的角度数)
	
第三行是总帧数:
	关键字:Totalframe
	参数意义:所有图层最大帧数上限
	
第四行开始每一行都是一个图层的信息描述。
对于每个图层:
	关键字:Layer[n]
	直接跟随关键字的一行的参数:
		图层名,起始帧，结束帧,发射器数目，激光数目，遮罩数目，反弹板数目，力场数目
	紧接着是上面一行按顺序描述的,发射器，激光，遮罩，反弹板，力场的描述，
	每个占用一行。
		如果图层名是empty则表示此层不用解析
	<发射器描述，位置从0开始依次是>
		0)发射器的ID(索引从0开始。显示是从1开始的)
		1)层ID
		2)绑定状态
		3)被绑定的发射器的ID(被绑定的发射器用来发射子弹。无效ID为-1)
		4)相对方向
		5)空的。已经抛弃使用
		6,7)发射器的坐标(上中位置)
		8,9)起始结束帧
		10,11)发射器的x,y坐标(-99998表示的是自身)
		12,13)发射点偏移半径和偏移角度
		14)####一个未知的坐标值####
		15)子弹的条数(一个能被半径影响的数据)。
		16)发射器发射子弹的周期。表示每多少帧发射一次。
		17)发射器发射子弹的时候炮口相对于起始方向的旋转角度(起始方向由条数决定)
		18)####另一个未知的坐标值####
		19)子弹发射范围
			发射器炮口方向计算方程：
				条件:
					n为正整数,0 ≤ range ≤ 360 * n,0 ≤ angle ≤ 360*n,
					0 < x < n的正整数
				说明:
					计算n个炮口的范围为range角度数的，
					每个炮口角度偏移为angle的发射角度的，
					第x个炮口的发射角度
				函数:
				emit_angle( n, range, angle, x):
					return (x + 0.5) * (range / n) + angle
		20,21)速度和速度方向
		22)####一个未知的坐标点####
		23,24)加速度和加速度方向
		25)####一个未知的坐标点####
		26)子弹的生命(帧速率为单位。子弹生命的结束帧应该有一个回调用来表现不同的子弹消亡形态)
		27)子弹的类型(可以用来自定义的字段了)
		28，29)宽高比(子弹的y,x方向的缩放倍率)
		30,31,32)rgb的颜色叠加运算值(setColor)
		33)透明度百分比
		34)子弹初始帧的旋转角度。(只是初始帧。子弹至少需要10帧才能被播放，所以在10帧中的某一帧开始进行旋转归零即可)
		35)####一个未知的坐标####
		36)是否使用发射器的速度方向
		37)子弹初始 速度(一个相对的速度)
		38)子弹初始速度方向(这个参数被编辑器UI忽略了，所以不应该使用)
		39)####一个未知的坐标####
		40，41)加速度，加速度方向
		42)####一个未知的坐标####
		43,44)x方向，y方向，加速度的倍率
		45-50)雾化效果(弹药发射时的发射光芒),
			消弹效果(弹药死亡的时候的渐隐),
			发射的时候高亮弹药，
			拖影效果，出屏消亡，无敌状态
		51)发射器的事件组:
			TODO:
		52)子弹的事件组:
			TODO:
		53,69)###未知参数全是0###
		70-72)是否受遮罩，反弹板，力场的控制
		73)###未知bool型参数###

		
	
		
		
		
		
		
					
						
		
			
		
		
		
