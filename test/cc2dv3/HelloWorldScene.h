#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cc2dv3/cc2dv3.h"
#include "cocos-ext.h"

USING_NS_CC_EXT;
class HelloWorld : public Layer, public CSB::BindHelperProto,
		public TableViewDelegate, public TableViewDataSource
{
public:
	HelloWorld();
	~HelloWorld();
public:
	virtual bool	init();
	static Scene* scene();
	CREATE_FUNC(HelloWorld)
    void		onTouchesBegan(const std::vector<Touch*>& touches, Event *unused_event);
    void		onTouchesMoved(const std::vector<Touch*>& touches, Event *unused_event);
public:
	/* 实现BindHelperProto */
	void*		emitBullet(int buID, const char* fname, const char* objdatana);
	void		recycleBullet(void* bu, int buID, const char* fname, const char* objdatana);
	int		bulletStateCheck(BulletStateCheck_t& bs);
	void		autoMachine(pos_t* out);
	void		objEndCB(const BindProto::bindele_t* pe);
public:
	void 		scrollViewDidScroll(ScrollView* view){}
	void		scrollViewDidZoom(ScrollView* view){}
	void 		tableCellTouched(TableView* table, TableViewCell* cell);
	Size		cellSizeForTable(TableView *table);
	ssize_t		numberOfCellsInTableView(TableView *table);
	TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
private:
	void 		btnsCB(Ref* pSender);
	void		btnFunc(int id);
	void		addAEmitterEle(const uint32_t NO, const float angleOff = 0, const pos_t* posOff = NULL);
	bool		initBind();
	void		bindThatCSBFile();
	void		showCSBFileListPanel();
	void		updateEmitIdxLbl();
	void		clearBulletCache();
private:
	Vec2 		_lastPos;
	struct CSB::BindProto::bind_t*	_myBind;
	unsigned int	_hit;
	Node*		_bulletBatch;
	// 提供一个子弹精灵的缓冲，用来和子弹精灵回收接口配合,实现子弹的再利用
	std::vector<Node*> 		_recyBulletVec;
	std::vector<cc2dv3::LoadFilesInfo>	_csbfileInfoVec;
	size_t		_nowEmitID;
	size_t		_nowcsbID;
	TableView*	_csbFileListView;
	float		_ty;
	bool		_enemyRotate;
	bool		_bindRun;
	bool		_useSC;
};

#endif // __HELLOWORLD_SCENE_H__
