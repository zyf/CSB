cocos2d-x v3的测试项目
===========================================================

由于v3的cocos2d-x项目组织方式改变，建立一个项目的资源开销过大
所以这里仅提供代码。资源请拷贝cc2dv2下的Resources目录。

-----


###对于使用CMake进行构建测试例子的平台可以进行如下操作:

在你的项目中和Classes同级的文件夹中新建文件夹CSB,并将如下文件拷贝其中:

	CSB/test/cc2dv3/CMakeLists.txt -> your_proj/CSB/CMakeLists.txt
	CSB/plugin/cc2dv3 -> your_proj/Classes/cc2dv3
	
	CSB/core/include -> your_proj/CSB/core/include
	CSB/core/src -> your_proj/CSB/core/src
	CSB/plugin/CP -> your_proj/CSB/plugin/CP
	CSB/plugin/SC -> your_proj/CSB/plugin/SC
	CSB/bind/cpp -> your_proj/CSB/bind/cpp
	
	拷贝CSB/test/cc2dv3下的源码覆盖到 your_porj/Classes/中
	
**特别注意：需将CMakeLists.txt放到your_proj/CSB下**
**CSB/plugin/cc2dv3放到your_proj/Clsses下**
		
按照"如何配置项目的CMakeFiles.jpg"配置你项目的根CMakeFiles文件
![](如何配置项目的CMakeFiles.jpg)

###对于使用其它方式构建项目的，请参考CMake的配置建议进行配置(哈哈，废话)
