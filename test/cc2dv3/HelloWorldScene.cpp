#include "HelloWorldScene.h"
USING_NS_CC;

// 角色
#define HERO_TAG	1
#define ENEMY_TAG	2
#define HIT_LBL_TAG	3
#define DEBUG_TAG	4
#define CSBNAME_LBL_TAG	5
#define EMITIDX_LBL_TAG	6

// 按钮
#define CLOSE_BTN	1
#define PRE_BTN		2
#define NEXT_BTN	3
#define STOP_BTN	4
#define UNBIND_BTN	5
#define PLAY_PAUSE_BTN	6
#define DEBUG_BTN	7
#define CLEAR_BULLETS	8
#define ENEMY_ROTATE_BTN	9
#define CHANGE_CP_BTN	10

#define CELL_W		150
#define CELL_H		24
/**
* @brief The MyBulletNode class 自定义的子弹。可以附加一些属性来用
*/
class MyBulletNode : public Sprite
{
public:
	MyBulletNode(int power) :m_power(power){}

	static MyBulletNode* create(int power, const char *pszFileName) {
		MyBulletNode* ret = new MyBulletNode(power);
		if (ret && ret->initWithFile(pszFileName)) {
			ret->autorelease();
			return ret;
		}
		CC_SAFE_DELETE(ret);
		return NULL;
	}

public:
	int 		m_power;
};

/*********************| functions in HelloWorld |*********************/

Scene* HelloWorld::scene()
{
	Scene *scene = Scene::create();
	HelloWorld *layer = HelloWorld::create();
	scene->addChild(layer);
	return scene;
}

HelloWorld::HelloWorld() : _bindRun(false), _enemyRotate(false), _nowEmitID(0),
        _csbFileListView(NULL), _nowcsbID(0), _myBind(NULL), _useSC(false)
{ }

HelloWorld::~HelloWorld()
{
	clearBulletCache();
}

#define BTN_MAKE_CODE(_name_, _bfname_, _tag_) \
	MenuItemImage* _name_ = MenuItemImage::create(_bfname_"_n.png", _bfname_"_s.png", CC_CALLBACK_1(HelloWorld::btnsCB, this));\
	pMenu->addChild(_name_, 0, _tag_)

bool HelloWorld::init()
{
	if (!CCLayer::init()) {
		return false;
	}
	Size visibleSize = Director::getInstance()->getVisibleSize();

	Menu* pMenu = Menu::create();
	pMenu->setPosition(Vec2::ZERO);
	this->addChild(pMenu, 1);

	// 关闭按钮
	BTN_MAKE_CODE(pCloseItem, "close", CLOSE_BTN);
	pCloseItem->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
	pCloseItem->setPosition(Vec2(visibleSize.width, 4));

	// 解除绑定按钮
	BTN_MAKE_CODE(pUb, "unbind", UNBIND_BTN);
	pUb->setAnchorPoint(pCloseItem->getAnchorPoint());
	pUb->setPosition(Vec2(pCloseItem->getPositionX(), pCloseItem->getBoundingBox().getMaxY() + 15));

	// 调试按钮
	BTN_MAKE_CODE(pDebug, "debug", DEBUG_BTN);
	pDebug->setAnchorPoint(pUb->getAnchorPoint());
	pDebug->setPosition(Vec2(pUb->getPositionX(), pUb->getBoundingBox().getMaxY() + 15));

	// 碰撞方式切换按钮
	BTN_MAKE_CODE(pCP, "cp", CHANGE_CP_BTN);
	pCP->setAnchorPoint(pDebug->getAnchorPoint());
	pCP->setPosition(Vec2(pDebug->getPositionX(), pDebug->getBoundingBox().getMaxY() + 15));
	_ty = pCP->getBoundingBox().getMaxY() + 8;

	// 旋转按钮
	BTN_MAKE_CODE(pRot, "rotate", ENEMY_ROTATE_BTN);
	pRot->setPosition(Vec2(pRot->getContentSize().width * 1.5f + 32, pRot->getContentSize().height * 1.5f + 32));
	// 播放暂停按钮
	BTN_MAKE_CODE(pPlayPause, "play", PLAY_PAUSE_BTN);
	pPlayPause->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
	pPlayPause->setPosition(Vec2(pRot->getPositionX(), pRot->getBoundingBox().getMaxY() + 8));
	// 停止按钮
	BTN_MAKE_CODE(pStop, "stop", STOP_BTN);
	pStop->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
	pStop->setPosition(Vec2(pRot->getPositionX(), pRot->getBoundingBox().getMinY() - 8));
	// 上一个按钮
	BTN_MAKE_CODE(pPre, "pre", PRE_BTN);
	pPre->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
	pPre->setPosition(Vec2(pRot->getBoundingBox().getMinX() - 8, pRot->getPositionY()));
	// 下一个按钮
	BTN_MAKE_CODE(pNext, "next", NEXT_BTN);
	pNext->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
	pNext->setPosition(Vec2(pRot->getBoundingBox().getMaxX() + 8, pRot->getPositionY()));

	// 用来绘制子弹的批次
	_bulletBatch = Node::create();
	_bulletBatch->setPosition(Vec2(0, 0));
	this->addChild(_bulletBatch);

	// 英雄
	Sprite* hero = Sprite::create("hero.png");
	hero->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 - 200));
	this->addChild(hero, 0, HERO_TAG);

	// 敌机
	Sprite* enemy = Sprite::create("enemy.png");
	this->addChild(enemy, 0, ENEMY_TAG);
	enemy->setPosition(Vec2(visibleSize.width / 2, visibleSize.height - 200));

	// 英雄被击中次数的显示
	_hit = 0;
	Label* hitLbl = Label::createWithCharMap("nums.png", 12, 22, '0');
	hitLbl->setString("0");
	hitLbl->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
	hitLbl->setPosition(Vec2(visibleSize.width / 2, 15));
	this->addChild(hitLbl, 0, HIT_LBL_TAG);

	// 正在使用的csb文件的名字显示
	Label* csbnameLbl = Label::createWithSystemFont("no file selected", "Arial", 25);
	csbnameLbl->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
	csbnameLbl->setPosition(Vec2(visibleSize.width / 2, visibleSize.height - 10));
	this->addChild(csbnameLbl, 0, CSBNAME_LBL_TAG);

	// 正在使用的csb文件中正在播放的弹道编号
	Label* emitIdxLbl = Label::createWithSystemFont("0", "Arial", 20);
	emitIdxLbl->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
	emitIdxLbl->setPosition(Vec2(csbnameLbl->getPositionX(), csbnameLbl->getBoundingBox().getMinY() - 10));
	this->addChild(emitIdxLbl, 0, EMITIDX_LBL_TAG);
	emitIdxLbl->setColor(Color3B(73, 205, 255));

	// 初始化加载所有的绑定文件
	if (!initBind()) {
		return false;
	}

	// 获取绑定的文件列表信息
	cc2dv3::instance()->loadFilesInfo(_csbfileInfoVec);

	this->setTouchEnabled(true);

	return true;
}

void HelloWorld::btnsCB(Ref* pSender)
{
	btnFunc(((Node*)pSender)->getTag());
}

void HelloWorld::btnFunc(int id)
{
	switch (id) {
	case PRE_BTN:
	case NEXT_BTN:
	{
		cc2dv3::LoadFilesInfo& pinfo = _csbfileInfoVec[_nowcsbID];
		if (1 == pinfo.objn)
			return;
		if (!strcmp(pinfo.fname.c_str(), "good4"))
			return;
		if (PRE_BTN == id) {
			if (0 == _nowEmitID) {
				_nowEmitID = pinfo.objn - 1;
			} else {
				--_nowEmitID;
			}

		} else {
			if (_nowEmitID == (pinfo.objn - 1)) {
				_nowEmitID = 0;
			} else {
				++_nowEmitID;
			}
		}
		updateEmitIdxLbl();
		// 切换弹幕
		cc2dv3::instance()->unbind(_myBind, false, false);;
		_myBind = NULL;
		bindThatCSBFile();
	}
		return;
	case ENEMY_ROTATE_BTN:
	{
		Sprite* enemy = (Sprite*)this->getChildByTag(ENEMY_TAG);
		if (_enemyRotate) {
			enemy->stopAllActions();
		} else {
			enemy->runAction(RepeatForever::create(RotateBy::create(5.0f, 360.0f)));
		}
		_enemyRotate = !_enemyRotate;
	}
		return;

	case PLAY_PAUSE_BTN:
		if (!_myBind) {
			bindThatCSBFile();
		}
		if (_myBind) {
			_bindRun = !_bindRun;
			cc2dv3::instance()->run(_myBind, _bindRun);
		}
		return;

	case CLEAR_BULLETS:
		cc2dv3::instance()->kakaAllBulletsOnce();
		return;

	case DEBUG_BTN:
	{
		DrawNode* d = (DrawNode*)this->getChildByTag(DEBUG_TAG);
		if (d) {
			cc2dv3::instance()->showBuDrawNode(false);
			this->removeChild(d);
		} else {
			d = DrawNode::create();
			this->addChild(d, 10, DEBUG_TAG);
			cc2dv3::instance()->showBuDrawNode(true);
			// 绘制飞机的
			Sprite* s = (Sprite*)this->getChildByTag(HERO_TAG);
			CCRECT_2_CP(s->getBoundingBox(), spcv);
			Vec2 ps[4];
			for (size_t i = 0; i < spcv.pnum; ++i) {
				ps[i].x = spcv.points[i].x;
				ps[i].y = COORD_Y_CONVERT(spcv.points[i].y);
			}
			d->drawPolygon(ps, spcv.pnum, Color4F(0, 1.0f, 0, 0.2f),
			               1, Color4F(1.0f, 0, 0, 1.0f));
		}
	}
		return;

	case CHANGE_CP_BTN:
		cc2dv3::instance()->unbindAll(true, true);
		_myBind = NULL;
		clearBulletCache();
		btnFunc(PLAY_PAUSE_BTN);
		_useSC = !_useSC;
		return;
	case UNBIND_BTN:
		if (_myBind) {
			// NOTE:参数3如果负值true，参数2将被忽略。绑定将立刻解除并清除该绑定管理的子弹
			cc2dv3::instance()->unbind(_myBind, true, false);
			_myBind = NULL;
		}
		showCSBFileListPanel();
		return;

	case STOP_BTN:
		if (_myBind) {
			cc2dv3::instance()->unbind(_myBind, false, false);;
			_myBind = NULL;
		}
		return;

	case CLOSE_BTN:
		// release cc2dv3 plugin
		cc2dv3::end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) || (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
		CCMessageBox("You pressed the close button. Windows Store Apps do not implement a close button.", "Alert");
#else
		CCDirector::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		exit(0);
#endif
#endif
		return;
	}
}

/* 生成子弹的精灵 */
void* HelloWorld::emitBullet(int buID, const char* fname, const char* objdatana)
{
	Node* ret = NULL;
	if (_recyBulletVec.empty()) {
		ret = (Node*)(MyBulletNode::create(rand() % 100, _useSC ? "big_bullet.png" : "bullet.png"));
		ret->retain();
	} else {
		// NOTE: 你可以根据buID, fanme等信息从不同的缓冲中获取子弹
		ret = _recyBulletVec.back();
		_recyBulletVec.pop_back();
	}
	_bulletBatch->addChild(ret);
	/* 添加到显示节点里面, 如果需要可以在这里执行动画 */
	return (void*)ret;
}

/* 回收子弹的精灵 */
void HelloWorld::recycleBullet(void* bu, int buID, const char* fname, const char* objdatana)
{
	// NOTE:你可以根据buID,fname等信息讲子弹存入不同的缓冲中
	Node* pnode = (Node*)bu;
	_bulletBatch->removeChild(pnode, true);
	_recyBulletVec.push_back(pnode);
}

int HelloWorld::bulletStateCheck(BulletStateCheck_t& bs)
{
	// NOTE:下面的代码展示了如何使用CSB提供的多边形进行碰撞检测
	// 你可以使用你自己的碰撞检测方式。pstate提供了足够的信息帮助你来完成
	// 只要返回正确的子弹状态即可
	Sprite* buSp = (Sprite*)bs.buObj;
	Sprite* s = (Sprite*)this->getChildByTag(HERO_TAG);
	bool hitted = false;
	if (_useSC) {
		// 使用SC进行碰撞检测(比CP更加复杂的碰撞。可以使用一组圆和多边形进行混合碰撞)
		// 这是使用圆形和一个三角形组合的图形来
		SC_STACK(orsc);
		float r = buSp->getContentSize().height / 2;
		float c = buSp->getContentSize().width / 2;
		float a[6] = { c - r, 0, r };
		SC_add_ele(&orsc, a, SC_ELE_CIRCLE, 3);
		a[0] = -c; a[1] = 0;
		a[2] = a[4] = c - r - r;
		a[3] = r;
		a[5] = -r;
		SC_add_ele(&orsc, a, SC_ELE_POLYGON, 3);
		// 此处注释使用外接矩形
		//		CCSIZE_2_SC(buSp->getContentSize(), orsc);
		SC_STACK(busc);
		SC_elebase_coll(bs.pele, &orsc, &busc);
		CCRECT_2_SC(s->getBoundingBox(), spsc);
		cc2dv3::instance()->drawDebugInfo(busc);
		hitted = SC_love_SC(&busc, &spsc);
	} else {
		// 使用CP进行碰撞检测
		// 获取子弹精灵的原始碰撞多边形
		CCSIZE_2_CP(buSp->getContentSize(), orcp);
		// 对原始多边形进行运算获取当下的多边形
		CP_STACK(bucp);
		CP_elebase_coll(bs.pele, &orcp, &bucp);
		// 获取飞机的边界矩形并转换成CSB的多边形
		CCRECT_2_CP(s->getBoundingBox(), spcp);
		// 绘制子弹的多边形
		cc2dv3::instance()->drawDebugInfo(bucp);
		// 子弹碰撞的判定
		hitted = CP_love_CP(&bucp, &spcp);
	}
	if (hitted) {
		// 发生了碰撞的处理
		++_hit;
		Label* pl = (Label*)this->getChildByTag(HIT_LBL_TAG);
		char buf[10] = { 0 };
		sprintf(buf, "%u", _hit);
		pl->setString(buf);
		// 我的自定义子弹
		MyBulletNode* mybu = (MyBulletNode*)bs.buObj;
		memset(buf, 0, sizeof(buf));
		sprintf(buf, "%d", mybu->m_power);
		Label* hurtNum = Label::createWithCharMap("nums.png", 12, 22, '0');
		hurtNum->setString(buf);
		hurtNum->setScale((mybu->m_power > 50 ? mybu->m_power : 50) / 100.0f);
		hurtNum->setColor(Color3B(255, 0, 0));
		hurtNum->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
		hurtNum->setPosition(Vec2(s->getPositionX() + CCRANDOM_MINUS1_1() * 50, s->getBoundingBox().getMaxY()));
		this->addChild(hurtNum);
		hurtNum->runAction(Sequence::create(
		                           MoveBy::create(0.5f, Vec2(0, 50)),
		                           CallFuncN::create([](Node* n) { n->removeFromParent();}),
		                   NULL));
		return BULLET_UPDATE_DEAD;
	}
	return BULLET_UPDATE_CONTINUE;
}

void HelloWorld::autoMachine(pos_t* out)
{
	Sprite* hero = (Sprite*)this->getChildByTag(HERO_TAG);
	out->x = hero->getPositionX();
	out->y = hero->getPositionY();
}

void HelloWorld::objEndCB(const CSB::BindProto::bindele_t* pe)
{
	// NOTE:可以在这里添加一些发射子弹的切换代码
	// 这个函数在弹幕的一次发射终止的时候被触发
}

void HelloWorld::tableCellTouched(TableView* table, TableViewCell* cell)
{
	_nowcsbID = cell->getIdx();
	_nowEmitID = 0;
	bindThatCSBFile();
}

Size HelloWorld::cellSizeForTable(TableView* table)
{
	return Size(CELL_W, CELL_H);
}

ssize_t HelloWorld::numberOfCellsInTableView(TableView* table)
{
	return (unsigned int)_csbfileInfoVec.size();
}

TableViewCell*HelloWorld::tableCellAtIndex(TableView* table, ssize_t idx)
{
	TableViewCell* pcell = table->dequeueCell();
	if (pcell) {
		Label* l = (Label*)pcell->getChildByTag(0);
		l->setString(_csbfileInfoVec[idx].fname.c_str());
	} else {
		pcell = new TableViewCell();
		Label* l = Label::createWithSystemFont(_csbfileInfoVec[idx].fname.c_str(), "Arial", 15);
		l->setAnchorPoint(Vec2::ZERO);
		l->setPosition(Vec2(5, 5));
		l->setColor(Color3B(255, 255, 255));
		pcell->addChild(l, 0, 0);
	}
	return pcell;
}
void HelloWorld::addAEmitterEle(const uint32_t NO, const float angleOff, const pos_t* posOff)
{
	ZERO_MALLOC(bele, CSB::BindProto::bindele_t);
	cc2dv3::CSMakeObjDataName(EMITTER, NO, bele->objdatana, sizeof(bele->objdatana));
	bele->flag = CSB::BindProto::OBJ_COLOR_BLEND | CSB::BindProto::BULLET_COLOR_BLEND | CSB::BindProto::UPDATE_PASSIVE;
	bele->angleOffset = angleOff;
	if (posOff) {
		memcpy(&bele->posOffset, posOff, sizeof(pos_t));
	}
	_myBind->eleList.push_back(bele);
}

bool HelloWorld::initBind()
{
	// 加载弹道文件
	static const char* csbfiles[] = {
	        "bullets/accele_speed.csb",
	        "bullets/action_time_step.csb",
	        "bullets/adir_auto.csb",
	        "bullets/automachine.csb",
	        "bullets/boss37.csb",
	        "bullets/boss_4.csb",
	        "bullets/buaction.csb",
	        "bullets/good4.csb",
	        "bullets/npc_11.csb",
	        "bullets/npc_22.csb",
	        "bullets/test01.csb",
	        "bullets/Test.csb",
	        "bullets/waveAndPariticle.csb",
	        NULL
	};
	size_t i = 0;
	while (csbfiles[i]) {
		CSB_NO_RET(cc2dv3::instance()->loadfile(csbfiles[i]), false);
		++i;
	}
	return i > 0;
}

void HelloWorld::bindThatCSBFile()
{
	_bindRun = false;
	// 生成绑定信息
	cc2dv3::LoadFilesInfo& pinfo = _csbfileInfoVec[_nowcsbID];

	Sprite* enemy = (Sprite*)this->getChildByTag(ENEMY_TAG);
	_myBind = CSB::BindProto::newBind(new SimpleBindProto(static_cast<CSB::BindHelperProto*>(this), enemy),
	                                  pinfo.fname.c_str());
	Label* csbnameLbl = (Label*)this->getChildByTag(CSBNAME_LBL_TAG);
	csbnameLbl->setColor(Color3B(0, 255, 0));
	csbnameLbl->setString(pinfo.fname.c_str());

	if (strcmp(pinfo.fname.c_str(), "good4")) {
		addAEmitterEle(_nowEmitID);
	} else {
		// 多组绑定示例
		addAEmitterEle(0);
		addAEmitterEle(1);
	}

	// 执行绑定
	if (!cc2dv3::instance()->bind(_myBind)) {
		cocos2d::log("bind failed");
		delete _myBind;
		_myBind = NULL;
		csbnameLbl->setColor(Color3B(255, 0, 0));
		return;
	}
	updateEmitIdxLbl();
	if (_csbFileListView) {
		_csbFileListView->setVisible(false);
		_csbFileListView->setTouchEnabled(false);
	}
}

/* 显示csb文件列表的选择框 */
void HelloWorld::showCSBFileListPanel()
{
	if (!_csbFileListView) {
		_csbFileListView = TableView::create(this, Size(CELL_W, CELL_H * 10));
		this->addChild(_csbFileListView, 100);
		_csbFileListView->setDelegate(this);
		Size visibleSize = Director::getInstance()->getVisibleSize();
		_csbFileListView->setPosition(Vec2(visibleSize.width - CELL_W, _ty));
		_csbFileListView->reloadData();
		_csbFileListView->setTouchEnabled(true);
	} else {
		_csbFileListView->setVisible(true);
		_csbFileListView->setTouchEnabled(true);
	}
}

void HelloWorld::updateEmitIdxLbl()
{
	if (_csbfileInfoVec.empty())
		return;
	Label* p = (Label*)this->getChildByTag(EMITIDX_LBL_TAG);
	char buf[32] = { 0 };
	sprintf(buf, "%zu/%zu", _nowEmitID + 1, _csbfileInfoVec[_nowcsbID].objn);
	p->setString(buf);
}


void HelloWorld::clearBulletCache()
{
	// 遍历recyBulletVec释放内存
	for (size_t i = 0; i < _recyBulletVec.size(); ++i) {
		_recyBulletVec[i]->release();
	}
	_recyBulletVec.clear();
}

void HelloWorld::onTouchesBegan(const std::vector<Touch*>& touches, Event *unused_event)
{
	if (_csbFileListView && _csbFileListView->isVisible())
		return;
	_lastPos = touches[0]->getLocation();

}

void HelloWorld::onTouchesMoved(const std::vector<Touch*>& touches, Event *unused_event)
{
	if (_csbFileListView && _csbFileListView->isVisible())
		return;
	Sprite* hero = (Sprite*)this->getChildByTag(HERO_TAG);
	Vec2 tmp = touches[0]->getLocation() - _lastPos;
	hero->setPosition(hero->getPosition() + tmp);
	_lastPos = touches[0]->getLocation();
	DrawNode* d = (DrawNode*)this->getChildByTag(DEBUG_TAG);
	if (d) {
		d->setPosition(d->getPosition() + tmp);
	}
}
