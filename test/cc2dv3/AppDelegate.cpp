#include "AppDelegate.h"
#include "HelloWorldScene.h"
#include "cc2dv3.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

void AppDelegate::initGLContextAttrs()
{
	GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

	GLView::setGLContextAttrs(glContextAttrs);
}

static int register_all_packages()
{
	return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	if(!glview) {
		glview = GLViewImpl::create("My Game");
		director->setOpenGLView(glview);
	}

	director->setDisplayStats(true);

	// NOTE: instead of Director::setTargetFPS
	cc2dv3::setTargetFPS(60);
	if (!cc2dv3::init(glview->getDesignResolutionSize(), NULL)) {
		cocos2d::log("init csb plugin failed");
		return false;
	}

	register_all_packages();
	auto scene = HelloWorld::scene();
	director->runWithScene(scene);

	return true;
}

void AppDelegate::applicationDidEnterBackground() {
	Director::getInstance()->stopAnimation();
	// SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

void AppDelegate::applicationWillEnterForeground() {
	Director::getInstance()->startAnimation();
	// SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
