#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "cc2dv2.h"

USING_NS_CC;
USING_NS_CC_EXT;

class HelloWorld : public CCLayer, public CSB::BindHelperProto,
		public CCTableViewDelegate, public CCTableViewDataSource
{
public:
	HelloWorld();
	~HelloWorld();
public:
	virtual bool	init();
	static CCScene* scene();
	CREATE_FUNC(HelloWorld)
	void 		ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent);
	void 		ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
public:
	/* 实现BindHelperProto */
	void*		emitBullet(int buID, const char* fname, const char* objdatana);
	void		recycleBullet(void* bu, int buID, const char* fname, const char* objdatana);
	int		bulletStateCheck(BulletStateCheck_t& bs);
	void		autoMachine(pos_t* out);
	void		objEndCB(const BindProto::bindele_t* pe);
public:
	void 		scrollViewDidScroll(CCScrollView* view){}
	void		scrollViewDidZoom(CCScrollView* view){}
	void 		tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	CCSize		cellSizeForTable(CCTableView *table);
	unsigned int	numberOfCellsInTableView(CCTableView *table);
	CCTableViewCell*	tableCellAtIndex(CCTableView *table, unsigned int idx);
private:
	void 		btnsCB(CCObject* pSender);
	void		btnFunc(int id);
	void		addAEmitterEle(const uint32_t NO, const float angleOff = 0, const pos_t* posOff = NULL);
	bool		initBind();
	void		bindThatCSBFile();
	void		showCSBFileListPanel();
	void		updateEmitIdxLbl();
	void		removeNode(CCNode*n);
	void		clearBulletCache();
private:
	CCPoint 			_lastPos;
	struct CSB::BindProto::bind_t*	_myBind;
	unsigned int			_hit;
	CCSpriteBatchNode*		_bulletBatch;
	// 提供一个子弹精灵的缓冲，用来和子弹精灵回收接口配合,实现子弹的再利用
	std::vector<CCNode*> 		_recyBulletVec;
	std::vector<cc2dv2::LoadFilesInfo>	_csbfileInfoVec;
	size_t				_nowEmitID;
	size_t				_nowcsbID;
	CCTableView*			_csbFileListView;
	float				_ty;
	bool				_enemyRotate;
	bool				_bindRun;
	bool				_useSC;
};

#endif // __HELLOWORLD_SCENE_H__
