#include "AppDelegate.h"
#include "HelloWorldScene.h"
#include "cc2dv2.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
	// initialize director
	CCDirector* pDirector = CCDirector::sharedDirector();
	CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();
	pDirector->setOpenGLView(pEGLView);
	pDirector->setDisplayStats(true);
	cc2dv2::setTargetFPS(60);
	// 初始化csb的插件
	CCSize s = pEGLView->getDesignResolutionSize();
	if (!cc2dv2::init(s, NULL)) {
		CCLog("init csb plugin failed")	;
		return false;
	}

//	CSB_CONFIG_SCREEN(10, 100, s.width - 200, s.height - 200);
	CCScene *pScene = HelloWorld::scene();
	pDirector->runWithScene(pScene);
	return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
	CCDirector::sharedDirector()->stopAnimation();

	// if you use SimpleAudioEngine, it must be pause
	// SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
	CCDirector::sharedDirector()->startAnimation();

	// if you use SimpleAudioEngine, it must resume here
	// SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
