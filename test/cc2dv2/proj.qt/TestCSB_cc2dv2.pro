TEMPLATE = app
CONFIG -= console
CONFIG -= app_bundle
CONFIG -= qt qml

CONFIG(debug, debug|release) {
DESTDIR = bin/debug/
}

CONFIG(release, debug|release) {
DESTDIR = bin/release/
DEFINES += NDEBUG
}

SOURCES += main.cpp \
	../Classes/AppDelegate.cpp	\
	../Classes/HelloWorldScene.cpp \
	../../../plugin/cc2dv2/cc2dv2.cpp \
    ../../../bind/cpp/csbbind.cpp \
    ../../../plugin/CP/convex_polygon.cpp \
    ../../../plugin/SC/simple_collision.cpp

HEADERS += main.h \
	../Classes/AppDelegate.h \
	../Classes/HelloWorldScene.h \
	../../../plugin/cc2dv2/cc2dv2.h \
    ../../../bind/cpp/csbbind.h \
    ../../../plugin/CP/convex_polygon.h \
    ../../../plugin/SC/simple_collision.h

# set a variable for config cocos2dx modules
# --------------------------------------------
# the simple audio engine
# COCOS2D-X_MODULES += CocosDenshion
# some extensions eg: CCTableView
COCOS2D-X_MODULES += extensions
# the box2d physics engine
# COCOS2D-X_MODULES += box2d
# the chipmunk physics engine
# COCOS2D-X_MODULES += chipmunk
# --------------------------------------------

include(cocos_lqt.pri)

CSB_PRO_ROOT = $$PWD/../../..

unix:!macx: LIBS += -L$$CSB_PRO_ROOT/lib/linux/ -lCSB

INCLUDEPATH += $$CSB_PRO_ROOT/core/include
INCLUDEPATH += $$CSB_PRO_ROOT/plugin/cc2dv2
INCLUDEPATH += $$CSB_PRO_ROOT/plugin/CP
INCLUDEPATH += $$CSB_PRO_ROOT/plugin/SC
INCLUDEPATH += $$CSB_PRO_ROOT/bind/cpp
QMAKE_CXXFLAGS += -O2
