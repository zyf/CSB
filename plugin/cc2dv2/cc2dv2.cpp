#include "cc2dv2.h"
#include "csb.h"
#include "csbmath.h"

/*********************| static data |*********************/
static cc2dv2*			__cc2dv2 = NULL;
static float			__def_fps = 0.0154f;


/*********************| extern C func |*********************/
CSB_DLL void CCSize2CP(const CCSize& s, convex_polygon_t* out)
{
	CSB_NO_VRET(out);
	out->pnum = 4;
	const float w2 = s.width / 2.0f;
	const float h2 = s.height / 2.0f;
	out->points[0].x = out->points[3].x = -w2;
	out->points[0].y = out->points[1].y = h2;
	out->points[1].x = out->points[2].x = w2;
	out->points[2].y = out->points[3].y = -h2;
}

CSB_DLL void CCRect2CP(const CCRect& rect, convex_polygon_t* out)
{
	CSB_NO_VRET(out);
	out->pnum = 4;
	CP_STACK_4(tmp);
	CCSize2CP(rect.size, &tmp);
	elebase_t e;
	CCPoint mid = ccp(rect.getMidX(), rect.getMidY());
	COORD_POS_CONVERT(mid, e.pos);
	e.rotate = 0;
	e.scale.xs = e.scale.ys = 1.0f;
	CP_elebase_coll(&e, &tmp, out);
}

CSB_DLL void CCSize2SC(const CCSize& s, simple_collision_t* out)
{
	CSB_NO_VRET(out);
	float ps[8];
	const float w2 = s.width / 2.0f;
	const float h2 = s.height / 2.0f;
	ps[0] = ps[6] = -w2;
	ps[1] = ps[3] = h2;
	ps[2] = ps[4] = w2;
	ps[5] = ps[7] = -h2;
	SC_add_ele(out, ps, SC_ELE_POLYGON, 4);
}


CSB_DLL void CCRect2SC(const CCRect& s, simple_collision_t* out)
{
	CSB_NO_VRET(out);
	SC_STACK(tmp);
	CCSize2SC(s.size, &tmp);
	elebase_t e;
	CCPoint mid = ccp(s.getMidX(), s.getMidY());
	COORD_POS_CONVERT(mid, e.pos);
	e.rotate = 0;
	e.scale.xs = e.scale.ys = 1.0f;
	SC_elebase_coll(&e, &tmp, out);
}

/*********************| function in SimpleBindProto |*********************/
void SimpleBindProto::updateEleBase(BindProto::bindele_t* p)
{
	CSB_NO_VRET(_bindObj);
	CCNode* bindNode = (CCNode*)_bindObj;
	csobj_t* po = p->pcs->pobj;
	// 注意坐标转换
	if (CSB_FLAG_TEST(p->flag, BindProto::UPDATE_PASSIVE) ||
	    !CSB_FLAG_TEST(p->flag, BindProto::FIRST_SETUPED)) {
		// 被动的获取信息
		CCPoint tp = bindNode->getPosition();
		tp.x += p->posOffset.x;
		tp.y += p->posOffset.y;
		COORD_POS_CONVERT(tp, po->ele.pos);
		po->ele.scale.xs = bindNode->getScaleX();
		po->ele.scale.ys = bindNode->getScaleY();
		csobj_set_rotate(po,bindNode->getRotation() + p->angleOffset,p->pcs->pobjdata->type);
		p->flag |= BindProto::FIRST_SETUPED;
	} else {
		// 设置主动位置
		CCPoint tp;
		COORD_POS_CONVERT(po->ele.pos, tp);
		tp.x -= p->posOffset.x;
		tp.y -= p->posOffset.y;
		bindNode->setPosition(tp);
		bindNode->setScaleX(po->ele.scale.xs);
		bindNode->setScaleY(po->ele.scale.ys);
		bindNode->setRotation(po->ele.rotate - p->angleOffset);
	}
}

void SimpleBindProto::bindObjRelease()
{
	if (_bindObj) {
		CCNode* p = (CCNode*)_bindObj;
		p->release();
		_bindObj = NULL;
	}
}

SimpleBindProto::SimpleBindProto(BindHelperProto* helper, CCNode* bindNode) :
	BindProto(helper, (void*)bindNode)
{
	if (bindNode) {
		bindNode->retain();
	}
}


SimpleBindProto::~SimpleBindProto()
{
	bindObjRelease();
}

void SimpleBindProto::updateBulletWithEle(const elebase_t& ele, void* pb)
{
	CCNode* pn = (CCNode*)pb;
	CCPoint tp;
	COORD_POS_CONVERT(ele.pos, tp);
	pn->setPosition(tp);
	pn->setRotation(ele.rotate);
	pn->setScaleX(ele.scale.xs);
	pn->setScaleY(ele.scale.ys);
}

/*********************| function in cc2dv2 |*********************/
cc2dv2::cc2dv2 () : _timeScale(0)
{
	this->onEnter();
	this->onEnterTransitionDidFinish();
	debugBuDrawNode = NULL;
}

cc2dv2::~cc2dv2 ()
{
	this->unscheduleUpdate();
	unbindAll(true);
	unloadAll();
	showBuDrawNode(false);
}

bool cc2dv2::init(const CCSize& winSize, CCScheduler* ps)
{
	bool r = CSBBindMan::init(winSize.width, winSize.height);
	// 生成单例
	if (r) {
		cc2dv2::instance()->setScheduler(ps);
	}
	return r;
}

void cc2dv2::end()
{
	if (CSBBindMan::end()) {
		__cc2dv2->onExit();
		delete __cc2dv2;
		__cc2dv2 = NULL;
	}
}

cc2dv2*	cc2dv2::instance()
{
	CSB_YES_RET(__cc2dv2, __cc2dv2);
	__cc2dv2 = new cc2dv2();
	return __cc2dv2;
}

void cc2dv2::setTargetFPS(unsigned int n)
{
	__def_fps = 1.0f / (float)n;
	CCDirector::sharedDirector()->setAnimationInterval(__def_fps);
	__def_fps -= 0.006f;
}

unsigned char*cc2dv2::loadCSBFileData(const char* path)
{
	unsigned long l;
	return CCFileUtils::sharedFileUtils()->getFileData(path, "rb", &l);
}

#define DRAW_CIRCLE_WITH_N	10
bool cc2dv2::sc_for_draw(int type, const float* pp, uint16_t pn, void* udata)
{
	CCDrawNode* dnode = (CCDrawNode*)udata;
	if (SC_ELE_CIRCLE == type) {
		// 使用多边形来模仿圆形
		CCPoint O(pp[0], COORD_Y_CONVERT(pp[1]));
		CCPoint ps[DRAW_CIRCLE_WITH_N];
		float tx, ty;
		static const float dg = 360.0f / DRAW_CIRCLE_WITH_N;
		for (int i = 0; i < DRAW_CIRCLE_WITH_N; ++i) {
			tx = pp[2];
			ty = 0;
			CSB_rotate(&tx, &ty, dg * i);
			ps[i].x = tx + O.x;
			ps[i].y = -ty + O.y;
		}
		dnode->drawPolygon(ps, DRAW_CIRCLE_WITH_N, ccc4f(1.0f, 0, 0, 0.1f),
		                   1, ccc4f(0, 1.0f, 0, 1.0f));
		// 画圆心
		dnode->drawDot(O, 2.0f, ccc4f(0, 1.0f, 0, 1.0f));
	} else {
		CCPoint ps[SC_PNUM_LIMIT / 2];
		for (uint16_t i = 0, j = 0; j < pn; ++i, j += 2) {
			ps[i].x = pp[j];
			ps[i].y = COORD_Y_CONVERT(pp[j + 1]);
		}
		dnode->drawPolygon(ps, pn / 2, ccc4f(1.0f, 0, 0, 0.1f),
		                   1, ccc4f(0, 1.0f, 0, 1.0f));
	}
	return true;
}

void cc2dv2::showBuDrawNode(bool show)
{
	CCScene* ps = CCDirector::sharedDirector()->getRunningScene();
	CSB_NO_VRET(ps);
	if (show) {
		if (debugBuDrawNode && debugBuDrawNode->getParent())
			return;
		debugBuDrawNode = CCDrawNode::create();
		ps->addChild(debugBuDrawNode);
	} else {
		if (!debugBuDrawNode)
			return;
		debugBuDrawNode->removeFromParent();
		debugBuDrawNode = NULL;
	}
}

void cc2dv2::drawDebugInfo(convex_polygon_t& cp)
{
	if (debugBuDrawNode) {
		CCPoint ps[CP_POINT_LIMIT];
		for (size_t i = 0; i < cp.pnum; ++i) {
			ps[i].x = cp.points[i].x;
			ps[i].y = COORD_Y_CONVERT(cp.points[i].y);
		}
		debugBuDrawNode->drawPolygon(ps, cp.pnum,ccc4f(1.0f, 0, 0, 0.1f),
		                              1, ccc4f(0, 1.0f, 0, 1.0f));
	}
}

void cc2dv2::drawDebugInfo(simple_collision_t& sc)
{
	if (debugBuDrawNode) {
		SC_foreach(&sc, cc2dv2::sc_for_draw, (void*)debugBuDrawNode);
	}
}

void cc2dv2::setScheduler(CCScheduler* scheduler)
{
	if (scheduler) {
		CCNode::setScheduler(scheduler);
	} else {
		CCNode::setScheduler(CCDirector::sharedDirector()->getScheduler());
	}
	this->scheduleUpdate();
}

void cc2dv2::update(float dt)
{
	_timeScale += dt;
	if (_timeScale >= __def_fps) {
		_timeScale = 0;
		if (debugBuDrawNode) {
			debugBuDrawNode->clear();
			// 绘制屏幕区域
			CCPoint ps[4];
			ps[0].x = _csb_screen.lx;
			ps[0].y = COORD_Y_CONVERT(_csb_screen.ly);
			ps[2].x = _csb_screen.rx;
			ps[2].y = COORD_Y_CONVERT(_csb_screen.ry);
			ps[1].x = _csb_screen.lx;
			ps[1].y = ps[2].y;
			ps[3].x = _csb_screen.rx;
			ps[3].y = ps[0].y;
			debugBuDrawNode->drawPolygon(ps, 4,ccc4f(0.0f, 0, 0, 0.0f),
			                             2, ccc4f(0, 0, 1, 1.0f));
		}
		CSBBindMan::updateOnce();
	}
}
