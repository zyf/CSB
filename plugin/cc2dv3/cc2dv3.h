﻿#ifndef __CC2DV3_H_
#define __CC2DV3_H_

/**
 * Desc: cocos2d-xV3系列的接口
 * Auth: 张宇飞
 * Date: 2015-04-14
 */

#include "cocos2d.h"
#include "csbbind.h"
#include "convex_polygon.h"
#include "simple_collision.h"

USING_NS_CC;
US_NS_CSB;

/*********************| 功能函数 |*********************/
/* 按照锚点0.5, 0.5计算出一个Size大小的多边形 */
extern CSB_DLL void Size2CP(const Size& s, convex_polygon_t* out);
/* 转化一个CCRect到多边形结构体, relative */
extern CSB_DLL void CCRect2CP(const Rect& rect, convex_polygon_t* out);
/* 按照锚点0.5, 0.5计算出一个Size大小的SC */
extern CSB_DLL void Size2SC(const Size& s, simple_collision_t* out);
/* 转化一个CCRect到碰撞体SC, relative */
extern CSB_DLL void CCRect2SC(const Rect& s, simple_collision_t* out);

#define CCSIZE_2_CP(_s, _out) \
	CP_STACK_4(_out); \
	Size2CP(_s, &_out)

#define CCRECT_2_CP(_rect, _out) \
	CP_STACK_4(_out); \
	CCRect2CP(_rect, &_out)

#define CCSIZE_2_SC(_s, _out) \
	SC_STACK(_out);\
	Size2SC(_s, &_out)

#define CCRECT_2_SC(_rect, _out) \
	SC_STACK(_out); \
	CCRect2SC(_rect, &_out)

/**
 * @brief 一个简单的绑定协议用来实现一些基础的功能
 */
class CSB_DLL SimpleBindProto : public BindProto
{
public:
	SimpleBindProto(CSB::BindHelperProto* helper, Node* bindNode);
	~SimpleBindProto();
protected:
	void 			updateBulletWithEle(const elebase_t& ele, void* pb);
	void 			updateEleBase(BindProto::bindele_t* p);
	void			bindObjRelease();
};


class CSB_DLL cc2dv3 : public CSBBindMan, public Node
{
private:
	cc2dv3 ();
	~cc2dv3 ();
public:
	/* 系统初始化 */
	static bool 		init(const Size& winSize, Scheduler* ps);
	static void		end();
	/* 获取单例 */
	static cc2dv3*		instance();
	static void		setTargetFPS(unsigned int n);

protected:
	unsigned char*		loadCSBFileData(const char* path);
	static bool		sc_for_draw(int type, const float* pp, uint16_t pn, void* udata);
public:
	void			update(float dt);
	void			setScheduler(Scheduler *scheduler);

public:
	void			showBuDrawNode(bool show);
	void			drawDebugInfo(convex_polygon_t& cp);
	void			drawDebugInfo(simple_collision_t& sc);
private:
	float			_timeScale;
	cocos2d::DrawNode*	debugBuDrawNode;
};


#endif /* end of include guard: __CC2DV3_H_ */
