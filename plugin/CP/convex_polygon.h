#ifndef CONVEX_POLYGON_H
#define CONVEX_POLYGON_H

/**
 * Desc: 简单的凸多边形碰撞
 * Auth: 张宇飞
 * Date: 2014-08-14
 */

#include "config.h"
#include "utils.h"
#include "csbtypes.h"
#ifdef __cplusplus
extern "C" {
#endif
/*********************| 结构声明和宏 |*********************/
/* 多边形就是一组点。填充的时候请顺时针填充 */
typedef struct {
	uint32_t	pnum;
	pos_t*		points;
} convex_polygon_t;

/* 多边形边数限制，不是强制的，建议吧 */
#define CP_POINT_LIMIT	16

/* 在栈上申请_name_名字的一组长度为_n_坐标点 */
#define CP_STACK_POINTS(_name_, _n_) \
	pos_t _name_[_n_];\
	memset(_name_, 0, sizeof(_name_))

/* 最常用的4边形 */
#define CP_STACK_4POINTS(_name_) CP_STACK_POINTS(_name_, 4)

/* 在栈上申请一个CP结构，包含一组可用的长度为CP_POINT_LIMIT的坐标点 */
#define CP_STACK(_name_) \
	CP_STACK_POINTS(_name_##p, CP_POINT_LIMIT);\
	convex_polygon_t _name_ = {0, _name_##p}

/* 最常用的4条边的 */
#define CP_STACK_4(_name_) \
	CP_STACK_4POINTS(_name_##p);\
	convex_polygon_t _name_ = {0, _name_##p}

/*********************| 外部接口 |*********************/
/* 检测点是否在凸多边形内 */
extern CSB_DLL bool	CP_hug_point(const pos_t* ppos, const convex_polygon_t* pcp);

/* 检测两个凸多变形是否发生了碰撞 */
extern CSB_DLL bool 	CP_love_CP(const convex_polygon_t* pcpA, const convex_polygon_t* pcpB);

/* 多边形的拷贝 */
extern CSB_DLL void	CP_copy(const convex_polygon_t* src, convex_polygon_t* dest);

/* 获得元素的多边形 */
extern CSB_DLL void 	CP_elebase_coll(const elebase_t* pele, const convex_polygon_t* oriCP, convex_polygon_t* out);

#ifdef __cplusplus
} /* end extern C */
#endif

#endif // CONVEX_POLYGON_H
