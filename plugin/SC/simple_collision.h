#ifndef SIMPLE_COLLISION_H_
#define SIMPLE_COLLISION_H_

/**
 * Desc: 简单的碰撞。实现了圆形和凸多边形的混合碰撞
 * Auth: 张宇飞
 * Date: 2015-02-26    
 */

#include "config.h"
#include "utils.h"
#include "csbtypes.h"
#ifdef __cplusplus
extern "C" {
#endif
/*********************| 结构声明和宏 |*********************/

#define SC_CNUM		3	// 一个圆需要的点个数
#define SC_MIN_PNUM	3	// 一个SC最少需要的点数
#define SC_PNUM_LIMIT	48	// 定义常用的情况
#define SC_MAX_ELE_NUM	(SC_PNUM_LIMIT / SC_MIN_PNUM)	// 一个SC结构最多能容纳的基础图形个数
#define SC_MAX_POL_NUM	(SC_MAX_ELE_NUM / 2)
#define SC_ID_NUM	((SC_PNUM_LIMIT >> 5) + 1)	// 使用32位的整形数来排列。取位上的值

#define SC_ELE_CIRCLE	0
#define SC_ELE_POLYGON	1

#define SC_STACK(_name_)  simple_collision_t _name_ = {0}

/**
 * @brief simple_collision_t 混合碰撞结构
 * 每3个点表示一个圆形: (x, y, r)
 * 多边形用点列来表示
 */

typedef struct {
	uint16_t	pn;			// 点的数量
	uint16_t	elen;			// 基础元素数量
	uint32_t	id[SC_ID_NUM];		// 点类型的索引,从低位向高位索引
	uint8_t		idx[SC_MAX_ELE_NUM];	// 圆形的点索引/多边形每个的点数量的2倍
	float		points[SC_PNUM_LIMIT];	// 点列
} simple_collision_t;

// 遍历函数指针, 返回true将继续执行，返回false将中断执行
typedef bool(*sc_for_func)(int type, const float* , uint16_t n, void* udata);
/*********************| 外部接口 |*********************/

/*  */
/**
 * @brief SC_add_ell 向SC结构中添加一个图元
 * @param type 图元的类型
 * @param n 圆的时候参数任意传，多边形的时候传递多边形的边数
 */
extern CSB_DLL bool	SC_add_ele(simple_collision_t* pSC, const float* points, int type, uint16_t n);

/* 遍历SC中的基础图形 */
extern CSB_DLL void	SC_foreach(const simple_collision_t* pSC, sc_for_func func, void* udata);

/* 检测点是否在SC内 */
extern CSB_DLL bool	SC_hug_point(const pos_t* ppos, const simple_collision_t* pSC);

/* 检测两个SC结构是否发生了交织 */
extern CSB_DLL bool 	SC_love_SC(const simple_collision_t* pSC1, const simple_collision_t* pSC2);

/* 获得元素变换后的SC */
extern CSB_DLL void 	SC_elebase_coll(const elebase_t* pele, const simple_collision_t* oriSC, simple_collision_t* out);

#ifdef __cplusplus
} /* end extern C */
#endif

#endif /* end of include guard: SIMPLE_COLLISION_H_ */
