# CrazyStorm弹幕文件的解析工具

	这是一个弹道解析器。CrazyStorm本身更算一个关卡编辑器。
	我要做的是利用CrazyStorm编辑弹道并表现出来。
	去除了力场，遮罩，反弹板，这三个元素。

## 目录结构

	├── bind	--> 按照编程语言划分目录的绑定实现
	├── core	--> libCSB的计算库
	├── doc		--> 文档
	├── lib		--> 按照不同的平台划分子目录的lib文件输出目录
	├── LICENSE	--> 开源许可证
	├── mbg2csb.bat	--> windows系统上的对mbg2csb.py的简单脚本封装，方便拖拽转换
	├── mbg2csb.py	--> 将CrazyStorm幕编辑器生成的mbg文件转化成给解析器用的csb文件
	├── plugin	--> 给不同平台或框架写的接口
	│   ├── cc2dv2	----> 	cocos2d-v2系列的接口
	│   ├── CP	----> 	一个简单的多边形碰撞算法库
	│   └── SC	----> 	一个简单的圆形和凸多边形混合碰撞的算法库
	├── README.md	--> 项目说明
	├── test	--> 测试例子
	│   ├── cc2dv2	----> 测试cocos2d-v2系列的接口的例子
	│   └── libCSB	----> libCSB基础功能测试
	└── todo.txt	--> 一些短期的计划

## 构建

核心库使用 cmake 来构建。使用动态库还是静态库取决于 BUILD_SHARED_LIBS 的参数值
```
cd core
cmake . -B build -DBUILD_SHARED_LIBS=<ON/OFF>
cmake --build build
```